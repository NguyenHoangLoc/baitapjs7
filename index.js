var numberArray = [];
function addNumber() {
  var inputEl = document.querySelector("#txt-number");
  if (inputEl.value == "") {
    return;
  }
  var number = inputEl.value * 1;

  numberArray.push(number);

  inputEl.value = "";
  document.getElementById("result").innerHTML = `${numberArray}`;
}
//1: tổng số dương
function sumPositive() {
  var positiveSum = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      positiveSum += numberArray[i];
    }
  }
  document.getElementById(
    "result1"
  ).innerHTML = ` tổng số dương:${positiveSum}`;
}
// đếm số dương
function countPositive() {
  var positiveCount = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      positiveCount++;
    }
  }
  document.getElementById("result2").innerHTML = ` ${positiveCount}`;
}

//3:số nhỏ nhất
function findMinNumber() {
  var minNumber = numberArray[0];
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] < minNumber) {
      minNumber = numberArray[i];
    }
  }
  document.getElementById("result3").innerHTML = ` Số nhỏ nhất: ${minNumber}`;
}

//4:số dương nhỏ nhất
function findMinPositive() {
  positiveArray = [];

  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      positiveArray.push(numberArray[i]);
    }
  }

  if (positiveArray.length == 0) {
    return (document.getElementById(
      "result4"
    ).innerHTML = ` không có số dương`);
  }

  var minPositive = positiveArray[0];

  for (i = 0; i < positiveArray.length; i++) {
    if (positiveArray[i] < minPositive) {
      minPositive = positiveArray[i];
    }
  }
  document.getElementById(
    "result4"
  ).innerHTML = ` Số dương nhỏ nhất: ${minPositive}`;
}

//5: tìm số chẵn cuối cùng
function findLastEven() {
  var lastEven = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] % 2 == 0) {
      lastEven = numberArray[i];
    }
  }
  document.getElementById(
    "result5"
  ).innerHTML = ` Số Chẵn cuối cùng: ${lastEven}`;
}
//6: đổi chỗ
function swapPlace() {
  var indexNum1 = document.getElementById("index1").value;
  var indexNum2 = document.getElementById("index2").value;
  var n = numberArray[indexNum1];
  numberArray[indexNum1] = numberArray[indexNum2];
  numberArray[indexNum2] = n;
  document.getElementById(
    "result6"
  ).innerHTML = ` mảng sau khi đổi: ${numberArray}`;
}
//7: sắp xếp mảng
function sortArrayUp() {
  var n;
  for (var i = 0; i < numberArray.length; i++) {
    for (var j = i + 1; j < numberArray.length; j++) {
      if (numberArray[i] > numberArray[j]) {
        n = numberArray[i];
        numberArray[i] = numberArray[j];
        numberArray[j] = n;
      }
    }
  }
  document.getElementById(
    "result7"
  ).innerHTML = ` mảng sau khi sắp xếp: ${numberArray}`;
}
//8:tìm số Nguyên Tố đầu tiên
function checkPrime(primeNumber) {
  var i;
  var sqrtNumber = Math.sqrt(primeNumber);
  if (primeNumber < 2) {
    return 0;
  }
  for (i = 2; i <= sqrtNumber; i++) {
    if (primeNumber % i == 0) {
      return 0;
    }
  }
  return 1;
}

function findFirstPrime() {
  var result = "";
  for (var i = 0; i < numberArray.length; i++) {
    if (checkPrime(numberArray[i]) == 1) {
      result = `Số nguyên tố đầu tiên:${numberArray[i]} `;
      break;
    }
  }
  document.getElementById("result8").innerHTML = result;
}
//9: Số Thực
var floatArray = [];
function addFloat() {
  var inputEl = document.querySelector("#txt-float-number");
  if (inputEl.value == "") {
    return;
  }
  var number = inputEl.value * 1;

  floatArray.push(number);

  inputEl.value = "";
  document.getElementById("resultFloatArray").innerHTML = `${floatArray}`;
}
function countFloat() {
  var count = 0;
  for (var i = 0; i < floatArray.length; i++) {
    if (Number.isInteger(floatArray[i]) == true) {
      count++;
    }
  }
  document.getElementById("result9").innerHTML = ` Số Nguyên: ${count}`;
}
//10: So Sánh
function compare() {
  var countPositive = 0;
  var countNegative = 0;
  var result = "";
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      countPositive++;
    }
    if (numberArray[i] < 0) {
      countNegative++;
    }
  }
  if (countPositive > countNegative) {
    result = " Số Dương >Số Âm";
  } else if (countPositive < countNegative) {
    result = "Số Dương< Số Âm";
  } else result = "Số Dương= Số Âm";
  document.getElementById("result10").innerHTML = result;
}
